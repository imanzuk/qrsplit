<?php

namespace QrSplit\Lib;

use Symfony\Component\HttpKernel\Log\LoggerInterface;
use Symfony\Component\Finder\Finder;
use Imanzuk\ZxingBundle\Service\ZxingBridge;
use QrSplit\Lib\Globals;

class QrSplitter {

    private $zxingBridge;
    private $logger;
    private $appPrefix;

    public function __construct(ZxingBridge $zxb, LoggerInterface $logger) {
        $this->zxingBridge = $zxb;
        $this->logger = $logger;
    }

    public function setAppPrefix($prefix) {
        $this->appPrefix = $prefix;
    }

    public function getAppPrefix() {
        return $this->appPrefix;
    }

    /**
     * a megadott pdf fájlt a benne lévő QR kódok szerint szétbontja
     * @param  string   $pdfFile     a feldolgozandó pdf fájlt
     * @param  boolean  $decodeOnly  csak QR kód felismerés, ne készítsen kimeneti fájlt
     * @param  string[] $data        a megtalált QR kódokat az oldalszám szerint tartalmazó tömb
     * @return void
     */
    public function process($pdfFile, $decodeOnly, &$data) {
        $logger = $this->logger;
        $pdfName = substr($pdfFile, strrpos($pdfFile, DIRECTORY_SEPARATOR)+1);
        $pdfName = $this->normalizeFilename($pdfName);
        $pdfPath = substr($pdfFile, 0, strrpos($pdfFile, DIRECTORY_SEPARATOR)+1) . $pdfName;
        $pngPath = substr($pdfPath, 0, strrpos($pdfPath, '.')+1) . 'png';
        $pngPath = $this->zxingBridge->getTempPath() .
            substr($pngPath, strrpos($pngPath, DIRECTORY_SEPARATOR));
        if (!($this->zxingBridge->convertPdfToPng($pdfFile, $pngPath) == 0)) {
            throw new \RuntimeException('converting pdf to png failed');
        }

        $cnt = 0;
        $qrCode = '';
        $timestamp = date('YmdHis');
        $fileNamePattern = substr($pngPath, strrpos($pngPath, DIRECTORY_SEPARATOR)+1);
        $fileNamePattern = substr($fileNamePattern, 0, strrpos($fileNamePattern, '.')) . '????.png';
        $pngFolder = substr($pngPath, 0, strrpos($pngPath, DIRECTORY_SEPARATOR));
        $logger->debug('finding files in '.$pngFolder.'; '.$fileNamePattern);
        $finder = new Finder;
        $finder->files()->name($fileNamePattern)->sortByName();
        foreach ($finder->in($pngFolder) as $pngFile) {
            $cnt++;
            $qrCode = $this->processPage(
                $qrCode, $pdfFile, $pngFile, $cnt,
                $decodeOnly, $timestamp, $data
            );
        }
    }

    /**
     * a paraméterben kapott png fájlban QR kódot keres;
     * a megtalált QR kód alapján a pdf fájl adott oldalát
     * a megfelelő fájlhoz csatolja, vagy új fájlt hoz létre
     * 
     * @param  string   $qrCode      (előző) QR kód
     * @param  string   $pdfFile     pdf fájl
     * @param  string   $pngFile     pdf fájl adott oldalát png formátumban tartalmazó fájl
     * @param  int      $cnt         oldalszám
     * @param  boolean  $decodeOnly  csak felismerés
     * @param  string[] $data        a megtalált QR kódokat az oldalszám szerint tartalmazó tömb
     * @return string                a felismert QR kód
     */
    public function processPage(
        $qrCode, $pdfFile, $pngFile, $cnt, $decodeOnly, $timestamp, &$data) {
        $logger = $this->logger;
        $logger->debug('processing page: ' . $pngFile->getRealpath());
        $qrCodeData = $this->zxingBridge->decode($pngFile);
        if ($qrCodeData > '') {
            if (($this->appPrefix == '') ||
                (strpos($qrCodeData, $this->appPrefix) === 0)) {
                $qrCodeData = str_replace($this->appPrefix, '', $qrCodeData);
                array_push($data, array($cnt => $qrCodeData));
            } else {
                $logger->debug('appPrefix not found, discarding qr code');
                $qrCodeData = '';
            }
        } else {
            $qrCodeData = $qrCode;
        }
        unlink($pngFile);
        if ($decodeOnly) return;
        $this->attachPage($pdfFile, $cnt, $qrCodeData, $timestamp);
        return $qrCodeData;
    }

    public function attachPage($pdfFile, $pageNumber, $qrCode, $timestamp) {
        $pdfName = substr($pdfFile, strrpos($pdfFile, DIRECTORY_SEPARATOR)+1);
        $pdfPath = substr($pdfFile, 0, strrpos($pdfFile, DIRECTORY_SEPARATOR)+1);
        $pdfSplit = sprintf('%s--%s--%s.pdf',
            substr($pdfName, 0, strrpos($pdfName, '.')),
            $timestamp,
            $qrCode > '' ? $this->normalizeQrCode($qrCode) : 'qrkodnelkul'
        );
        $tempFile = tempnam('', $pdfName);
        $this->logger->debug(sprintf(
            'attaching page %d of %s to %s',
            $pageNumber,
            $pdfFile,
            $pdfSplit
        ));
        $res = false;
        if (file_exists($pdfPath.$pdfSplit)) { //már van csatolt fájl
            $cmd = sprintf(
                'pdftk A="%s" B="%s" cat A1-end B%d-%d output "%s"',
                $pdfPath.$pdfSplit,
                $pdfFile,
                $pageNumber, $pageNumber,
                $tempFile
            );
        } else { //még nincs csatolt fájl
            $cmd = sprintf(
                'pdftk "%s" cat %d-%d output "%s"',
                $pdfFile,
                $pageNumber, $pageNumber,
                $tempFile
            );
        }
        $this->logger->debug(sprintf("running pdftk: %s", $cmd));
        $out = array();
        $res = null;
        exec($cmd . ' 2>&1', $out, $res); //stderr átirányítása az stdout-ra, így megjelenik a logban
        if ($res != 0) {
            throw new \RuntimeException('pdftk failed');
        }
        rename($tempFile, $pdfPath.$pdfSplit);
    }

    /**
     * ékezetes/egzotikus fájlnév normalizálása
     * @param  string $string
     * @return string
     *
     * http://stackoverflow.com/a/2668970/1788567
     */
    public function normalizeFilename($string) {
        //TODO: a preg_replace sor miért nem kezeli?
        $string = str_replace('̈', '', $string); //ö
        $string = str_replace('́', '', $string); //á
        $string = str_replace('̋', '', $string); //ű
        $string = htmlentities($string, ENT_QUOTES, 'UTF-8');
        $string = preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', $string);
        $string = html_entity_decode($string, ENT_QUOTES, 'UTF-8');
        $string = preg_replace(array('~[^0-9a-z.]~i', '~[ -]+~'), ' ', $string); //a kiterjesztés előtti pont miatt
        return trim($string, ' -');
    }

    /**
     * normalizálja a QR kódot, hogy fájlnévben felhasználható legyen
     * @param  [type] $string [description]
     * @return [type]         [description]
     *
     * http://stackoverflow.com/a/3380159/1788567
     */
    public function normalizeQrCode($string) {
        $bad = array_merge(
            array_map('chr', range(0,31)),
            array("<", ">", ":", '"', "/", "\\", "|", "?", "*")
        );
        $result = trim(str_replace($bad, "_", $string));
        return $result;
    }

}
