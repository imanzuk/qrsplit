<?php

namespace QrSplit\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class QrsplitCommand extends Command
{
    protected $sc;

    public function __construct(ContainerBuilder $sc) {
        parent::__construct();
        $this->sc = $sc;
    }

    protected function configure()
    {
        $this
            ->setName('qrsplit')
            ->setDescription('A felismert QR kódok alapján szétbontja a megadott pdf fájlt.')
            ->addArgument(
                'pdfFile',
                InputArgument::REQUIRED,
                'A feldolgozandó pdf fájl.'
            )
            ->addOption(
                'decodeOnly',
                null,
                InputOption::VALUE_NONE,
                'Nem készít kimeneti fájlokat, csak felismeri a QR kódokat.'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $decodeOnly = $input->getOption('decodeOnly');
        $pdfFile = $input->getArgument('pdfFile');
        if (!file_exists($pdfFile)) {
            $output->writeln('<error>A megadott fájl nem létezik.</error>');
            return 1;
        }
        $data = array();
        try {
            $qrsplitter = $this->sc->get('qrsplitter');
            $qrsplitter->process($pdfFile, $decodeOnly, $data);
            $msg = sprintf(
                'A fájl felbontása megtörtént. QR kódok: %s',
                json_encode($data)
            );
            $output->writeln("<info>$msg</info>");
        } catch (Exception $e) {
            $msg = sprintf(
                'A fájl felbontása nem sikerült. Hibaüzenet: %s',
                $e->getMessage()
            );
            $output->writeln("<error>$msg</error>");
            return 2;
        }
    }

}
