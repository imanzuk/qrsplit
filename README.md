QrSplit
=======

Parancssoros Symfony alkalmazás, ami a megadott pdf fájlt a benne található QR kódok alapján felbontja további pdf fájlokra.

Követelmények:

    PHP 5.3.x
    composer (http://getcomposer.org/doc/00-intro.md)
    GhostScript (a pdf fájl oldalainak png-re konvertálásához)
    pdftk (pdf fájl műveletekhez)
    nailgun (opcionális; a Java VM memóriában tartásához)

Konfigurációs lehetőségeket ld. a bin/console-ban.

Az elkészült fájlok nevének formátuma:

    <eredeti_fajlnev>--<YmdHis>--<QR_kod>.pdf
    <eredeti_fajlnev>--<YmdHis>--qrkodnelkul.pdf (árva oldalak)

Első futtatás előtti teendők:

    php composer.phar install
    mkdir log

Futtatás:

    bin/console qrsplit /utvonal/a/feldolgozando/pdf_fajlhoz.pdf

További opciók, paraméterek:

    bin/console help qrsplit
